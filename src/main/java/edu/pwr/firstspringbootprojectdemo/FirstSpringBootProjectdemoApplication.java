package edu.pwr.firstspringbootprojectdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstSpringBootProjectdemoApplication {

	public static void main(String[] args) {
		System.out.println("Hello World!");
		SpringApplication.run(FirstSpringBootProjectdemoApplication.class, args);
	}

}
